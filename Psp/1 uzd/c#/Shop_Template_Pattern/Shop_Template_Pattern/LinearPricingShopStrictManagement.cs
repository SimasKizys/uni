﻿using System;
using System.Collections.Generic;

namespace Shop_Template_Pattern
{
    public class LinearPricingShopStrictManagement: BaseShop
    {

        protected override void GenerateNewEmployeeRatings()
        {
            foreach (var shopEmployee in Employees)
            {
                shopEmployee.EmployeeRating = 0;
                if (shopEmployee.Wager < 300) shopEmployee.EmployeeRating += 2;
                else if (shopEmployee.Position == WorkingPosition.Manager && shopEmployee.WorkingHours > 8) shopEmployee.EmployeeRating += 3;

                if (shopEmployee.Experience < 3) shopEmployee.EmployeeRating -= 2;
                else if (0 < shopEmployee.Experience && shopEmployee.Experience < 5) shopEmployee.EmployeeRating += 2;
                else shopEmployee.EmployeeRating += 5;

                if (shopEmployee.EmployeeRating < 2) shopEmployee.Position = WorkingPosition.Janitor;
            }
        }

        protected override ShopEmployee GetBestCandidateForEmpoyeeOfTheWeek(List<ShopEmployee> employees)
        {
            ShopEmployee bestCandidate = null;
            bool foundCandidate = false;

            foreach (var shopEmployee in employees)
            {
                if (shopEmployee.EmployeeRating > 6
                    && shopEmployee.Position != WorkingPosition.Janitor
                    && DateTime.Now.Subtract(shopEmployee.WorkStartDate) > TimeSpan.FromDays(365))
                {
                    bestCandidate = shopEmployee;
                    foundCandidate = true;
                }
            }

            if (foundCandidate)
                return bestCandidate;
            else return null;
        }

        protected override ShopEmployee GetBestCandidateForManager(List<ShopEmployee> candidates)
        {
            ShopEmployee bestCandidate = candidates[0];
            bool foundCandidate = false;
            foreach (var shopEmployee in candidates)
            {

                if (shopEmployee.EmployeeRating > 7
                    && bestCandidate.EmployeeRating < shopEmployee.EmployeeRating
                    && shopEmployee.Experience > 8
                    && shopEmployee.WorkingHours > 8)
                {
                    bestCandidate = shopEmployee;
                    foundCandidate = true;
                }
            }
            if (foundCandidate)
                return bestCandidate;
            else return null;
        }
        protected override double CalculateDiscount(Item item, Customer buyer)
        {
            double discount = 0;
            if (DateTime.Now.Subtract(item.GoodUntill) > TimeSpan.FromDays(5)) discount += 25;
            if (buyer.VipCustomer) discount *= 1.1;
            return discount;
        }

        protected override bool CheckPriceValidity(Item item)
        {
            if (item.Price > 150) return false;
            if (item.AmountAvailable < 0) return false;
            return true;
        }

        protected override void CorrectItemPrice(Item item)
        {
            if (item.AmountAvailable < 15) item.Price *= 1.2;
            if (item.Price > 50) item.Price *= 0.8;
            if (item.AmountAvailable > 200) item.Price *= 0.4;
            if (item.AmountAvailable > 100) item.Price *= 0.8;
            if (item.GoodUntill < DateTime.Now) item.Price *= 0.2;
        }

        public LinearPricingShopStrictManagement(string name, List<Item> items, List<ShopEmployee> employees, List<Customer> customers) : base(name, items, employees, customers)
        {
        }
    }
}