﻿using System.CodeDom;
using System.Collections.Generic;

namespace Shop_Template_Pattern
{
    public abstract class BaseShop
    {
        public string Name { get; protected set; }
        public ShopEmployee Manager { get; protected set; }
        public ShopEmployee EmoployeeOfTheWeek { get; protected set; }
        public List<Item> ItemsForSale;
        public List<ShopEmployee> Employees;
        public List<Customer> CurrentCustomers;

        public BaseShop(string name, List<Item> items, List<ShopEmployee> employees, List<Customer> customers )
        {
            Name = name;
            ItemsForSale = items;
            Employees = employees;
            CurrentCustomers = customers;
        }

        protected void SellAnItem(Item item, Customer buyer)
        {
            if (buyer.Money >= item.Price * (1 - CalculateDiscount(item, buyer)))
                buyer.Money -= item.Price * (1 - CalculateDiscount(item, buyer));
            item.AmountAvailable -= 1;
        }
        protected  void ReviewPrices()
        {
            foreach (var item in ItemsForSale)
            {
                CorrectItemPrice(item);
                if (!CheckPriceValidity(item))
                    ItemsForSale.Remove(item);
            }
        }
        protected void ElectNewManager()
        {
            ShopEmployee newManager = GetBestCandidateForManager(Employees);
            if (Employees.Contains(newManager) && newManager != null)
            {
                Manager.Position = WorkingPosition.Janitor;
                Manager = newManager;
                newManager.Position = WorkingPosition.Manager;
            }
        }

        protected void ElectNewEmployeeOfTheWeek()
        {
            ShopEmployee newEmployeeOfTheWeek = GetBestCandidateForEmpoyeeOfTheWeek(Employees);
            if (newEmployeeOfTheWeek != null)
            {
                EmoployeeOfTheWeek = newEmployeeOfTheWeek;
                newEmployeeOfTheWeek.EmployeeRating += 1;
            }
        }

        protected abstract void GenerateNewEmployeeRatings();
        protected abstract ShopEmployee GetBestCandidateForEmpoyeeOfTheWeek(List<ShopEmployee> employees);
        protected abstract ShopEmployee GetBestCandidateForManager(List<ShopEmployee> candidates);
        protected abstract double CalculateDiscount(Item item, Customer buyer);
        protected abstract bool CheckPriceValidity(Item item);
        protected abstract  void CorrectItemPrice(Item item);


    }
}