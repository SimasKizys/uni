﻿using System;
using System.Collections.Generic;

namespace Shop_Template_Pattern
{
    public class ComplexPricingShopLooseManagement : BaseShop
    {

        protected override void GenerateNewEmployeeRatings()
        {
            foreach (var shopEmployee in Employees)
            {
                shopEmployee.EmployeeRating = 0;
                if (shopEmployee.Experience > 3) shopEmployee.EmployeeRating = 5;
                else if (shopEmployee.Experience > 5) shopEmployee.EmployeeRating = 10;
                if (DateTime.Now.Subtract(shopEmployee.WorkStartDate) > new TimeSpan(365, 0, 0, 0))
                    shopEmployee.EmployeeRating += 5;
            }
        }

        protected override ShopEmployee GetBestCandidateForEmpoyeeOfTheWeek(List<ShopEmployee> employees)
        {
            ShopEmployee bestCandidate = employees[0];


            foreach (var shopEmployee in employees)
            {
                if (shopEmployee.EmployeeRating > bestCandidate.EmployeeRating &&
                    shopEmployee.Position != WorkingPosition.Manager)
                    bestCandidate = shopEmployee;
            }

            return bestCandidate;
        }

        protected override ShopEmployee GetBestCandidateForManager(List<ShopEmployee> candidates)
        {
            ShopEmployee bestCandidate = candidates[0];
            for (int i = 1; i < candidates.Count - 1; i++)
            {
                if (candidates[i].EmployeeRating > bestCandidate.EmployeeRating) bestCandidate = candidates[i];
            }
            return bestCandidate;
        }
        protected override double CalculateDiscount(Item item, Customer buyer)
        {
            double discountPercent = 0;
            if (item.AmountAvailable > 100) discountPercent += 8;
            if (item.GoodUntill < DateTime.Now - TimeSpan.FromDays(3)) discountPercent += 50;
            if (buyer.VipCustomer)
                discountPercent *= 1.2;
            return discountPercent;
        }

        protected override bool CheckPriceValidity(Item item)
        {
            if (item.GoodUntill < DateTime.Now) return false;
            if (item.Price > 300) return false;
            return true;
        }

        protected override void CorrectItemPrice(Item item)
        {
            if (item.GoodUntill < DateTime.Now) item.Price *= 0.2;
            if (item.AmountAvailable < 5) item.Price *= 1.4;
            if (item.AmountAvailable > 150) item.Price *= 0.5;
            if (item.AmountAvailable > 90) item.Price *= 0.9;
        }
        public ComplexPricingShopLooseManagement(string name, List<Item> items, List<ShopEmployee> employees, List<Customer> customers) : base(name, items, employees, customers)
        {
        }
    }
}