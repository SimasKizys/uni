﻿using System.CodeDom;
using System.Collections.Generic;

namespace Shop_Template_Pattern
{
    public abstract class Kiosk
    {
        public Person KioskWorker;
        public List<Item> ItemsForSale;

        public Kiosk(Person worker, List<Item> items )

        {
            KioskWorker = worker;
            ItemsForSale = items;
        }

        public void SellAnItem(Item item, Customer buyer)
        {
            if (item.Price <= buyer.Money)
            {
                buyer.Money = -item.Price;
                item.AmountAvailable -= 1;
            }
        }
        protected abstract bool CheckPriceValidity(Item item);
        protected abstract void CorrectItemPrice(Item item);
    }
}