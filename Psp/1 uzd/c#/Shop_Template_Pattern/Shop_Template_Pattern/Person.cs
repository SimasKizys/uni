﻿using System.Diagnostics.Contracts;
using System.Security.Policy;

namespace Shop_Template_Pattern
{
    public class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        public Person(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }
    }
}