﻿using System;
using System.Collections.Generic;

namespace Shop_Template_Pattern
{
    public class LinearPricingKiosk : Kiosk

    {
        public LinearPricingKiosk(Person worker, List<Item> items) : base(worker, items)
        {
        
        }
        protected override bool CheckPriceValidity(Item item)
        {
            if (item.Price > 150) return false;
            if (item.AmountAvailable < 0) return false;
            return true;
        }

        protected override void CorrectItemPrice(Item item)
        {
            if (item.AmountAvailable < 15) item.Price *= 1.2;
            if (item.Price > 50) item.Price *= 0.8;
            if (item.AmountAvailable > 200) item.Price *= 0.4;
            if (item.AmountAvailable > 100) item.Price *= 0.8;
            else if (item.GoodUntill < DateTime.Now) item.Price *= 0.2;

        }
    }
}