﻿using System;
using System.Collections.Generic;

namespace Shop_Template_Pattern
{
    public class ComplexPricingKiosk : Kiosk
    {
        public ComplexPricingKiosk(Person worker, List<Item> items) : base(worker, items)
        {
        }

        protected override bool CheckPriceValidity(Item item)
        {
            if (item.GoodUntill < DateTime.Now) return false;
            if (item.Price > 300) return false;
            return true;
        }

        protected override void CorrectItemPrice(Item item)
        {
            if (item.GoodUntill < DateTime.Now) item.Price *= 0.2;
            if (item.AmountAvailable < 5) item.Price *= 1.4;
            if (item.AmountAvailable > 150) item.Price *= 0.5;
            if (item.AmountAvailable > 90) item.Price *= 0.9;
        }
    }
}