﻿namespace Shop_Template_Pattern
{
    public enum WorkingPosition
    {
        Assistant,
        SeniorAssistant,
        Janitor,
        Cashier,
        Manager,
        Consultant
    }
}