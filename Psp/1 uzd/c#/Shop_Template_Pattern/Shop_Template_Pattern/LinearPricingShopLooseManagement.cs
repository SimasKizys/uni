﻿using System;
using System.Collections.Generic;

namespace Shop_Template_Pattern
{
    public class LinearPricingShopLooseManagement : BaseShop
    {


        protected override void GenerateNewEmployeeRatings()
        {
            foreach (var shopEmployee in Employees)
            {
                shopEmployee.EmployeeRating = 0;
                if (shopEmployee.Experience > 3) shopEmployee.EmployeeRating = 5;
                else if (shopEmployee.Experience > 5) shopEmployee.EmployeeRating = 10;
                if (DateTime.Now.Subtract(shopEmployee.WorkStartDate) > new TimeSpan(365, 0, 0, 0))
                    shopEmployee.EmployeeRating += 5;
            }
        }

        protected override ShopEmployee GetBestCandidateForEmpoyeeOfTheWeek(List<ShopEmployee> employees)
        {
            ShopEmployee bestCandidate = employees[0];


            foreach (var shopEmployee in employees)
            {
                if (shopEmployee.EmployeeRating > bestCandidate.EmployeeRating &&
                    shopEmployee.Position != WorkingPosition.Manager)
                    bestCandidate = shopEmployee;
            }

            return bestCandidate;
        }

        protected override ShopEmployee GetBestCandidateForManager(List<ShopEmployee> candidates)
        {
            ShopEmployee bestCandidate = candidates[0];
            for (int i = 1; i < candidates.Count - 1; i++)
            {
                if (candidates[i].EmployeeRating > bestCandidate.EmployeeRating) bestCandidate = candidates[i];
            }
            return bestCandidate;
        }


        protected override double CalculateDiscount(Item item, Customer buyer)
        {
            double discount = 0;
            if (DateTime.Now.Subtract(item.GoodUntill) > TimeSpan.FromDays(5)) discount += 25;
            if (buyer.VipCustomer) discount *= 1.1;
            return discount;
        }

        protected override bool CheckPriceValidity(Item item)
        {
            if (item.Price > 150) return false;
            if (item.AmountAvailable < 0) return false;
            return true;
        }

        protected override void CorrectItemPrice(Item item)
        {
            if (item.AmountAvailable < 15) item.Price *= 1.2;
            if (item.Price > 50) item.Price *= 0.8;
            if (item.AmountAvailable > 200) item.Price *= 0.4;
            if (item.AmountAvailable > 100) item.Price *= 0.8;
            else if (item.GoodUntill < DateTime.Now) item.Price *= 0.2;

        }

        public LinearPricingShopLooseManagement(string name, List<Item> items, List<ShopEmployee> employees, List<Customer> customers) : base(name, items, employees, customers)
        {
        }
    }
}