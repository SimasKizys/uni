﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop_Template_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {


            List<Item> items = new List<Item>();

            Item testItem1 = new Item(ItemType.Apple, 0.6, 50, DateTime.Now);
            Item testItem2 = new Item(ItemType.Bread, 4, 18, DateTime.Now);
            Item testItem3 = new Item(ItemType.Egg, 0.1, 50, DateTime.Now);
            Item testItem4 = new Item(ItemType.Shampoo, 5, 99, DateTime.Now);

            items.Add(testItem1);
            items.Add(testItem2);
            items.Add(testItem3);
            items.Add(testItem4);

            ShopEmployee emp1 = new ShopEmployee("Jonas", "Kazlauskas");
            ShopEmployee emp2 = new ShopEmployee("Saulius", "Kazlauskas");
            ShopEmployee emp3 = new ShopEmployee("Viktoras", "Kazlauskas");
            ShopEmployee emp4 = new ShopEmployee("Marius", "Kazlauskas");

            List<ShopEmployee> employees = new List<ShopEmployee>();

            employees.Add(emp1);
            employees.Add(emp2);
            employees.Add(emp3);
            employees.Add(emp4);

            List<Customer> customers=new List<Customer>();

            customers.Add(new Customer(20 ,3, new TimeSpan(12), false));
            customers.Add(new Customer(30, 1, new TimeSpan(12), true));
            customers.Add(new Customer(10, 1, new TimeSpan(12), false));
            customers.Add(new Customer(18, 1, new TimeSpan(12), false));


            ComplexPricingShopLooseManagement rimi = new ComplexPricingShopLooseManagement("RIMI",items,employees, customers);
            ComplexPricingShopStrictManagement norfa = new ComplexPricingShopStrictManagement("Norfa",items,employees,customers);
            LinearPricingShopLooseManagement maxima = new LinearPricingShopLooseManagement("Maxima", items,employees,customers);



        }
    }
}
