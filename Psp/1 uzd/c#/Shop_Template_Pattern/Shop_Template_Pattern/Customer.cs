﻿using System;

namespace Shop_Template_Pattern
{
    public class Customer
    {
        public double Money { get; set; }
        public int DesireToBuy { get; set; }
        public TimeSpan TimeAvailable { get; set; }
        public bool VipCustomer { get; set; }


        public Customer(double money, int desire, TimeSpan time, bool vip)
        {
            Money = money;
            DesireToBuy = desire;
            TimeAvailable = time;
            VipCustomer = vip;

        }
    }

}