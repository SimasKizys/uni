﻿using System;

namespace Shop_Template_Pattern
{
    public class Item
    {
        public double Price { set; get; }
        public ItemType Type { get; set; }
        public int AmountAvailable { get; set; }
        public DateTime GoodUntill { get; set; }

        public Item(ItemType type, double price, int amount, DateTime date)
        {
            Price = price;
            Type = type;
            AmountAvailable = amount;
            GoodUntill = date;
        }
    }
}