﻿using System;
using System.CodeDom;

namespace Shop_Template_Pattern
{
    public class ShopEmployee : Person
    {
        public int Experience { get; set; }
        public int WorkingHours { get; set; }
        public double Wager { get; set; }
        public WorkingPosition Position { get; set; }
        public int EmployeeRating { get; set; }
        public DateTime WorkStartDate { get; set; }

        public ShopEmployee(string name, string surname) : base(name, surname)
        {
        }
    }
}