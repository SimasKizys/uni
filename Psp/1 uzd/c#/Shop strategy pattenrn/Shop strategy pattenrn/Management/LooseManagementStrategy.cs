﻿using System;
using System.Collections.Generic;

namespace Shop_strategy_pattenrn.Management
{
    public class LooseManagementStrategy : EmployeeManagementStrategy
    {
        public  void GenerateNewEmployeeRatings(List<ShopEmployee> employees)
        {
            foreach (var shopEmployee in employees)
            {
                shopEmployee.EmployeeRating = 0;
                if (shopEmployee.Experience > 3) shopEmployee.EmployeeRating = 5;
                else if (shopEmployee.Experience > 5) shopEmployee.EmployeeRating = 10;
                if (DateTime.Now.Subtract(shopEmployee.WorkStartDate) > new TimeSpan(365, 0, 0, 0))
                    shopEmployee.EmployeeRating += 5;
            }
        }

        public  ShopEmployee GetBestCandidateForManger(List<ShopEmployee> candidates)
        {
            ShopEmployee bestCandidate = candidates[0];
            for (int i = 1; i < candidates.Count - 1; i++)
            {
                if (candidates[i].EmployeeRating > bestCandidate.EmployeeRating) bestCandidate = candidates[i];
            }
            return bestCandidate;
        }

        public  ShopEmployee GetBestCandidateForEmployeeOfTheWeek(List<ShopEmployee> employees)
        {
            ShopEmployee bestCandidate = employees[0];


            foreach (var shopEmployee in employees)
            {
                if (shopEmployee.EmployeeRating > bestCandidate.EmployeeRating &&
                    shopEmployee.Position != WorkingPosition.Manager)
                    bestCandidate = shopEmployee;
            }

            return bestCandidate;

        }
    }
}
