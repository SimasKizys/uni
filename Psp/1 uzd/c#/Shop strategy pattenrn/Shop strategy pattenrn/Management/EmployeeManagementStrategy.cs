﻿using System.Collections.Generic;

namespace Shop_strategy_pattenrn.Management
{
   public interface EmployeeManagementStrategy
    {

         void GenerateNewEmployeeRatings(List<ShopEmployee> employees);
         ShopEmployee GetBestCandidateForManger(List<ShopEmployee> candidates);
         ShopEmployee GetBestCandidateForEmployeeOfTheWeek(List<ShopEmployee> employees);

    }
}
