﻿using System;
using System.Collections.Generic;

namespace Shop_strategy_pattenrn.Management
{
    public class StrictManagementStrategy : EmployeeManagementStrategy
    {
        public  void GenerateNewEmployeeRatings(List<ShopEmployee> employees)
        {
            foreach (var shopEmployee in employees)
            {
                shopEmployee.EmployeeRating = 0;
                if (shopEmployee.Wager < 300) shopEmployee.EmployeeRating += 2;
                else if (shopEmployee.Position == WorkingPosition.Manager&&shopEmployee.WorkingHouse>8) shopEmployee.EmployeeRating += 3;

                if (shopEmployee.Experience < 3) shopEmployee.EmployeeRating -= 2;
                else if (0 < shopEmployee.Experience && shopEmployee.Experience < 5) shopEmployee.EmployeeRating += 2;
                else shopEmployee.EmployeeRating += 5;

                if(shopEmployee.EmployeeRating<2) shopEmployee.Position = WorkingPosition.Janitor;
            }
        }

        public  ShopEmployee GetBestCandidateForManger(List<ShopEmployee> candidates)
        {
            ShopEmployee bestCandidate = candidates[0];
            bool foundCandidate = false;
            foreach (var shopEmployee in candidates)
            {

                if (shopEmployee.EmployeeRating > 7
                    && bestCandidate.EmployeeRating < shopEmployee.EmployeeRating
                    && shopEmployee.Experience > 8
                    && shopEmployee.WorkingHouse > 8)
                {
                    bestCandidate = shopEmployee;
                    foundCandidate = true;
                }
            }
            if (foundCandidate)
                return bestCandidate;
            else return null;
        }

        public  ShopEmployee GetBestCandidateForEmployeeOfTheWeek(List<ShopEmployee> employees)
        {
            ShopEmployee bestCandidate = null;
            bool foundCandidate = false;

            foreach (var shopEmployee in employees)
            {
                if (shopEmployee.EmployeeRating > 6
                    && shopEmployee.Position != WorkingPosition.Janitor
                    && DateTime.Now.Subtract(shopEmployee.WorkStartDate) > TimeSpan.FromDays(365))
                {
                    bestCandidate = shopEmployee;
                    foundCandidate = true;
                }
            }

            if (foundCandidate)
                return bestCandidate;
            else return null;
        }
    }
}
