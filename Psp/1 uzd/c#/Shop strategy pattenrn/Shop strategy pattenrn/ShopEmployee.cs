﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop_strategy_pattenrn
{
    public class ShopEmployee : Person
    {
        public int Experience { get; set; }
        public int WorkingHouse { get; set; }
        public double Wager { get; set; }
        public WorkingPosition Position { get; set; }
        public int EmployeeRating { get; set; }
        public DateTime WorkStartDate { get; set; }

        public ShopEmployee(string name, string surname) : base(name, surname)
        {

        }
    }
}
