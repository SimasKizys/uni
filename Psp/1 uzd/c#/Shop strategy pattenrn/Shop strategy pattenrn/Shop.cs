﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop_strategy_pattenrn.Management;
using Shop_strategy_pattenrn.Pricing;

namespace Shop_strategy_pattenrn
{
    public class Shop
    {
        public string Name;
        public ShopEmployee Manager;
        public ShopEmployee EmployeeOfTheWeek;
        public List<Item> ItemsForSale;
        public List<ShopEmployee> Employees;
        public List<Customer> CurrentCustomers;
        public EmployeeManagementStrategy ManagementStrategy;
        public PricingStrategy ShopPricingStrategy;

        public Shop(string name, EmployeeManagementStrategy managementStrategy, PricingStrategy pricingStrategy,
                    List<Item> itemsForSale, List<ShopEmployee> workers)
        {
            Name = name;
            ManagementStrategy = managementStrategy;
            ShopPricingStrategy = pricingStrategy;
            ItemsForSale = itemsForSale;
            Employees = workers;
            EmployeeOfTheWeek = null;
            ElectNewManger();
        }

        public void ElectNewManger()
        {
            ShopEmployee newManager = ManagementStrategy.GetBestCandidateForManger(Employees);
            if (Employees.Contains(newManager) && newManager != null)
            {
                Manager.Position = WorkingPosition.Janitor;
                Manager = newManager;
                newManager.Position = WorkingPosition.Manager;
            }
        }

        public void ElectNewEmployeeOfTheWeek()
        {
            ShopEmployee newEmployeeOfTheWeek = ManagementStrategy.GetBestCandidateForEmployeeOfTheWeek(Employees);
            if (newEmployeeOfTheWeek != null)
            {
                EmployeeOfTheWeek = newEmployeeOfTheWeek;
                newEmployeeOfTheWeek.EmployeeRating += 1;
            }
        }
        public void SellAnItem(Item item, Customer buyer)
        {
            if (buyer.Money >= item.Price*(1 - ShopPricingStrategy.CalculateDiscount(item, buyer)))
                buyer.Money -= item.Price*(1 - ShopPricingStrategy.CalculateDiscount(item, buyer));
            item.AmountAvailable -= 1;
        }

        public void ReviewPrices()
        {
            foreach (var item in ItemsForSale)
            {
                ShopPricingStrategy.CorrectPrice(item);
                if (!ShopPricingStrategy.CheckPriceValidity(item))
                    ItemsForSale.Remove(item);
            }
        }
    }

}
