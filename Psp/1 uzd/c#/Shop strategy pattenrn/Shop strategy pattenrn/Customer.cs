﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop_strategy_pattenrn
{
    public class Customer : Person
    {
        public double Money { get; set; }
        public int DesireToBuy { get; set; }
        public DateTime TimeAvailable { get; set; }
        public Boolean VipClient { get; set; }


    }
    
}
