﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop_strategy_pattenrn
{
   public enum WorkingPosition
    {
        Assistant,
        SeniorAssistant,
        Janitor,
        Cashier,
        Manager,
        Consultant
    }
}
