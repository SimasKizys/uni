﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop_strategy_pattenrn
{
    public class Item
    {
        public double Price { get; set; }
        public ItemType Type { get; set; }
        public int AmountAvailable { get; set; }
        public DateTime GoodUntill { get; set; }


        public Item(ItemType type, double price, int amount, DateTime date)
        {
            Price = price;
            Type = type;
            AmountAvailable = amount;
            GoodUntill = date;
        }
        
    }
}
