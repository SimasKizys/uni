﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using Shop_strategy_pattenrn.Pricing;

namespace Shop_strategy_pattenrn
{
    public class Kiosk
    {
        public Person KioskWorker;
        public List<Item> ItemsForSale;
        public PricingStrategy KioskPricingStrategy;

        public Kiosk(Person worker, List<Item> items, PricingStrategy strategy)
        {
            this.KioskWorker = worker;
            ItemsForSale = items;
            KioskPricingStrategy = strategy;
        }


        public void SellAnItem(Item item, Customer buyer)
        {
            if (item.Price <= buyer.Money)
            {
                buyer.Money -= item.Price;
                item.AmountAvailable -= 1;
            }
        }
    }
}