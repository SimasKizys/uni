﻿namespace Shop_strategy_pattenrn.Pricing
{
    public interface PricingStrategy
    {
        double CalculateDiscount(Item item, Customer customer);
        bool CheckPriceValidity(Item item);
        void CorrectPrice(Item item);
    }
}
