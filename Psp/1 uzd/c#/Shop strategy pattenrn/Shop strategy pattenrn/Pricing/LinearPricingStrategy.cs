﻿using System;

namespace Shop_strategy_pattenrn.Pricing
{
    public class LinearPricingStrategy : PricingStrategy
    {
        public double CalculateDiscount(Item item, Customer customer)
        {
            double discount = 0;
            if (DateTime.Now.Subtract(item.GoodUntill) > TimeSpan.FromDays(5)) discount += 25;
            if (customer.VipClient) discount *= 1.1;
            return discount;

        }

        public bool CheckPriceValidity(Item item)
        {
            if (item.Price > 150) return false;
            if (item.AmountAvailable < 0) return false;
            return true;
        }

        public void CorrectPrice(Item item)
        {
            if (item.AmountAvailable < 15) item.Price *= 1.2;
            if (item.Price > 50) item.Price *= 0.8;
            if (item.AmountAvailable > 200) item.Price *= 0.4;
            if (item.AmountAvailable > 100) item.Price *= 0.8;
            else if (item.GoodUntill < DateTime.Now) item.Price *= 0.2;
        }
    }
}