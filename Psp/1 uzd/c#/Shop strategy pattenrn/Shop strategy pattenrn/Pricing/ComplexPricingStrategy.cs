﻿using System;

namespace Shop_strategy_pattenrn.Pricing
{
    public class ComplexPricingStrategy : PricingStrategy
    {
        public double CalculateDiscount(Item item, Customer customer)
        {
            double discount = 0;

            if (customer.VipClient) discount *= 1.2;
            if (DateTime.Now.Subtract(item.GoodUntill) > TimeSpan.FromDays(3)) discount += 18;

            if (DateTime.Now.Day % 8 == 0) discount = 80;
            return discount;
        }

        public bool CheckPriceValidity(Item item)
        {

            if (item.Price > 150) return false;
            if (item.AmountAvailable < 0) return false;
            return true;
        }

        public void CorrectPrice(Item item)
        {

            if (item.AmountAvailable < 20) item.Price *= 1.3;
            if (item.Price > 75) item.Price *= 0.8;

            if (item.AmountAvailable > 100) item.Price *= 0.8;
            if (item.AmountAvailable > 200) item.Price *= 0.4;

            if (item.GoodUntill < DateTime.Now) item.Price *= 0.2;
        }
    }
}