﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop_strategy_pattenrn.Management;
using Shop_strategy_pattenrn.Pricing;

namespace Shop_strategy_pattenrn
{
    class Program
    {
        static void Main(string[] args)
        {

            EmployeeManagementStrategy strictStrategy = new StrictManagementStrategy();
            EmployeeManagementStrategy looseStrategy = new LooseManagementStrategy();

            PricingStrategy linearPricingStrategy = new LinearPricingStrategy();
            PricingStrategy complexPricingStrategy= new ComplexPricingStrategy();

            List<Item> items = new List<Item>();

            Item testItem1 = new Item(ItemType.Apple, 0.6,50, DateTime.Now);
            Item testItem2 = new Item(ItemType.Bread, 4, 18, DateTime.Now);
            Item testItem3 = new Item(ItemType.Egg, 0.1, 50, DateTime.Now);
            Item testItem4 = new Item(ItemType.Shampoo, 5, 99, DateTime.Now);

            items.Add(testItem1);
            items.Add(testItem2);
            items.Add(testItem3);
            items.Add(testItem4);

            ShopEmployee emp1 = new ShopEmployee("Jonas", "Kazlauskas");
            ShopEmployee emp2 = new ShopEmployee("Saulius", "Kazlauskas");
            ShopEmployee emp3 = new ShopEmployee("Viktoras", "Kazlauskas");
            ShopEmployee emp4= new ShopEmployee("Marius", "Kazlauskas");

            List<ShopEmployee> employees = new List<ShopEmployee>();
            
            employees.Add(emp1);
            employees.Add(emp2);
            employees.Add(emp3);
            employees.Add(emp4);


            Shop Rimi = new Shop("RIMI", strictStrategy, linearPricingStrategy, items, employees);
            Shop Maxima = new Shop("MAXIMA", looseStrategy,complexPricingStrategy, items, employees );
            Kiosk kiosk =  new Kiosk(employees[0], items, linearPricingStrategy);





        }
    }
}
