#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <omp.h>

double GetTime() {
   struct timeval laikas;
   gettimeofday(&laikas, NULL);
   double rez = (double)laikas.tv_sec+(double)laikas.tv_usec/1000000;
   return rez;
}

//-----------------------------------------------------------------------------

int main(int argc, char *argv[]) {

   omp_set_num_threads(8);
   srand(time(NULL));
   int *A;
   int N, M, t, n;
   
   // Load matrix
   double ts_load = GetTime(); 
   
   char inputfile[255]; sprintf(inputfile, "%s", argv[1]);

   FILE *f = fopen(inputfile, "r");

   fscanf(f, "%d %d", &N, &M);
   
   A = new int[N*M];

   for (int i=0; i<N*M; i++) fscanf(f, "%d", A+i);
   fclose(f);
   double tf_load = GetTime();
   printf("Matrix loaded in %.2f sec\n", tf_load - ts_load);

   // Sorting the matrix   
   double ts_sort = GetTime();
   double startTime = GetTime();
  // #pragma omp parallel 
   //{
   int S=0;
   #pragma omp parallel reduction(+:S) private(t,n) 
   {
   	double ts_th = GetTime();
   #pragma omp for nowait schedule(dynamic)
   for (int k=0; k<N; k++) {

      n = 0;
      while (A[k*M+n] != 0 && n < M) n++;
      for (int i=0; i<n-1; i++) {
         for (int j=0; j<n-1; j++) {
            if (A[k*M+j] > A[k*M+j+1]) {
               t = A[k*M+j];
               A[k*M+j] = A[k*M+j+1];
               A[k*M+j+1] = A[k*M+j];
            }
         } 
      }
      S = S + A[2499];
   }
   	printf("Done in: %f \n", GetTime()-ts_th);
	}
	printf("SUM: %d \n", S);
  // printf("DONE IN:%f \n",GetTime()-startTime);
   //}
   double tf_sort = GetTime();
   printf("Matrix sorted in: %.2f sec\n", tf_sort - ts_sort);

   // Save sorted matrix to file
   double ts_save = GetTime();
   char outputfile[255]; sprintf(outputfile, "sorted_%s", inputfile);
   f = fopen(outputfile, "w");
   fprintf(f, "%d %d\n", N, M);
   for (int i=0; i<N; i++) {
      for (int j=0; j<M; j++) fprintf(f, "%d ", A[i*M+j]);
      fprintf(f, "\n");
   }
   fclose(f);
   double tf_save = GetTime();
   printf("Sorted matrix saved to file in %.2f sec\n", tf_save - ts_save);
}