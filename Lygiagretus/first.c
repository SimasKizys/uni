#include <stdio.h>
#include <math.h>
#include <omp.h>





int main()
{
   omp_set_num_threads(8);
   #pragma omp parallel
     {
		printf("Hello world from thread %d out of %d\n",
		omp_get_thread_num(), omp_get_num_threads());
     }
}


